package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		//Servlet Context Parameter (Brand Name in Web.xml)
		ServletContext srvContext = getServletContext();
		String brandName = srvContext.getInitParameter("brand_name");
		
		//System Properties (Firstname)
		String firstname = System.getProperty("fname");
		
		//Http Session (Lastname)
		HttpSession session = req.getSession();
		String lastname = session.getAttribute("lastname").toString();
		
		//ServletContext (Email)
		String email = (String) srvContext.getAttribute("Email");
		
		//Url Rewriting via SendRedirect (Contact)
		String contact = req.getParameter("contact");
		
		
		PrintWriter out = res.getWriter();
		out.println("<h1>" +brandName + "</h1><p>Firstname: " + firstname + "</p>"
				+ "<p>Lastname: " + lastname + "</p><p>Contact: " + contact + 
				"</p><p>Email: " + email + "</p>");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		//Send Redirect (Contact)
		String contact = req.getParameter("contact");
		res.sendRedirect("details?contact="+ contact);
		
	}
	
	
	
	public void destroy() {
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been destroy. ");
		System.out.println("******************************************");
	}
}

package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		//System Properties
		//String fname = req.getParameter("fname");
		//System.getProperties().put("fname", fname);
		//String firstname = System.getProperty("fname");
		
		
		//HttpSession
		//String lastname = req.getParameter("lname");
		//HttpSession session = req.getSession();
		//session.setAttribute("lastname", lastname);
		
		//Send Redirect
		//String contact = req.getParameter("contact");
		//res.sendRedirect("details?contact="+ contact);
		
		
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		//System Properties (Firstname)
		String firstname = req.getParameter("fname");
		System.getProperties().put("fname", firstname);
		
		//HttpSession (Lastname)
		String lastname = req.getParameter("lname");
		HttpSession session = req.getSession();
		session.setAttribute("lastname", lastname);
		
		//ServletContext (Email)
		ServletContext srvContext = getServletContext();
		String email = req.getParameter("email");
		srvContext.setAttribute("Email", email);
		
		//Send Redirect (Contact)
		RequestDispatcher rd = req.getRequestDispatcher("details");
		rd.forward(req, res);
		
	}
	
	
	public void destroy() {
		System.out.println("******************************************");
		System.out.println(" UserServlet has been destroy. ");
		System.out.println("******************************************");
	}
}
